# SILISv4
Sistema de Liquidacion de Sueldos - Nicolas Ribeiro

Este sistema de liquidacion de sueldos es un proyecto personal mio que busca automatizar la liquidacion de sueldos en empresas de medio-gran porte. Esta version es la version final e incluye la liquidacion automatica de trabajadores Mensuales y Jornaleros. 

En futuras actualizaciones ire añadiendo la posibilidad de programar las liquidaciones a una fecha y hora deseada y se podran liquidar despidos, licencias, vacacionales, aguinaldos y descuentos opcionales que tenga el trabajador.

Espero que les guste y sean libres de ver el codigo!

Actualmente liquida 5000 trabajadores en 1 hora reloj. No utilizar el software en entornos de produccion, tiene muchos errores de calculo que ire solucionando con el tiempo.


NOTA: Para que el sistema funcione, es necesario crear una carpeta llamada "Recibos" en el mismo directorio donde descarguen los archivos. Esa carpeta es la carpeta destino donde se colocaran los recibos de sueldo que se liquiden, alli se almacenaran los recibos generados en PDF.

